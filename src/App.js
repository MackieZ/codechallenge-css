import React from 'react'
import styled from 'styled-components'
import { createGlobalStyle } from 'styled-components'

import Home from './components/Home'
import DayOne from './components/DayOne'
import DayTwo from './components/DayTwo'
import DayThree from './components/DayThree'
import DayFour from './components/DayFour'
import DayFive from './components/DayFive'

const GlobalStyle = createGlobalStyle`
  body {
  margin: 0;
  font-family: 'Courier New', 'Courier', monospace;
}

`

const Layout = styled.div`
  width: 100vw;
  height: 100vh;
`

const Container = styled.div`
  padding: 6rem;
  display: flex;
  justify-content: center;
`

const ShowCaseLayout = styled.div`
  display: grid;
  grid-template-columns: 425px 425px;
  grid-gap: 50px;
  justify-items: center;

  @media (max-width: 960px) {
    grid-template-columns: 425px;
  }
`

const ShowBox = styled.div`
  width: 400px;
  height: 400px;
  display: block;
`

function App() {
  const challenges = [DayOne, DayTwo, DayThree, DayFour, DayFive].reverse()

  const challengeList = challenges.map((ChallengeComp, index) => (
    <ShowBox key={index}>
      <ChallengeComp />
    </ShowBox>
  ))

  return (
    <Layout>
      <Home />
      <Container>
        <ShowCaseLayout>{challengeList}</ShowCaseLayout>
      </Container>
      <GlobalStyle />
    </Layout>
  )
}

export default App
