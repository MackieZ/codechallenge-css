import React, { useState } from 'react'
import styled, { keyframes } from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #a7c0ea;
`

const Button = styled.button`
  outline: none;
  padding: 16px 20px;
  border: 0;
  border-radius: 5px;
  color: #5493fa;
  background: #fff;
  cursor: pointer;
  font-size: 16px;
  position: relative;
  overflow: hidden;
  box-sizing: border-box;
  z-index: 0;
  transition: all 0.3s;

  &:hover {
    background: #f5f5f5;
  }

  &:active {
    box-shadow: 0px 5px 10px -4px #555;
  }
`

const rippleAnimate = keyframes`
  from {
    width: 0px;
    padding-top: 0;
    border-radius: 50%;
  }

  to {
    width: 150%;
    padding-top: 100%;
    border-radius: 0;
  }
`

const Ripple = styled.div`
  top: -50%;
  left: -50%;
  display: block;
  position: absolute;
  border-radius: 0%;
  width: 150%;
  padding-top: 100%;
  background: #dfdfdf;
  z-index: -1;
  transform-origin: 50% 50%;

  animation: ${rippleAnimate} ease-in-out 0.25s;
`

/**
 * DayTwo ripple button inspired by material UI button
 */
const DayTwo = () => {
  const [animate, setAnimate] = useState(false)
  const ANIMATION_TIME = 350

  function setAnimation() {
    setAnimate(true)

    setTimeout(() => setAnimate(false), ANIMATION_TIME)
  }

  return (
    <Wrapper>
      <Button onClick={setAnimation}>Click me !{animate && <Ripple />}</Button>
    </Wrapper>
  )
}

export default DayTwo
