import React from 'react'
import styled, { keyframes } from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #555;
`

const Button = styled.div`
  box-sizing: border-box;
  width: 60px;
  height: 60px;
  background: #5cb85c;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  position: relative;
`

const animateBlink = keyframes`
  from {
    width: 100%;
    height: 100%;
    border: 4px solid white;
  }

  to {
    width: 150%;
    height: 150%;
    border: 1px solid transparent;
  }
`

const ButtonAnimate = styled.div`
  border: 5px solid white;
  border-radius: 50%;
  z-index: 99;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  position: absolute;

  animation: ${animateBlink} infinite cubic-bezier(0.27, 0.23, 0.11, 0.78) 1s;
`

/**
 * DayOne button blink animation
 */
const DayOne = () => (
  <Wrapper>
    <Button>
      <ButtonAnimate />
    </Button>
  </Wrapper>
)

export default DayOne
