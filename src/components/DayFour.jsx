import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #8f9dbf;
`

const CubeWrapper = styled.div`
  position: relative;
  left: -135px;
  transform: translate3d(0, 0, -100px) rotateX(70deg) rotateZ(-45deg);
  transform-style: preserve-3d;
`

const CubeFrontSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #f5f5f5;
  transform: rotateY(0deg) translate3d(0, 0, 100px);
`

const CubeLeftSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #efefef;
  transform: rotateY(90deg) translate3d(0, 0, -100px);
`

const CubeRightSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #eeeeee;
  transform: rotateY(90deg) translate3d(0, 0, 100px);
`

const CubeTopSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #eeeeee;
  transform: rotateX(90deg) translate3d(0, 0, 100px);
`

const CubeDownSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #ddd;
  transform: rotateX(90deg) translate3d(0, 0, -100px);
`

const CubeBackSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #f5f5f5;
  transform: rotateY(0deg) translate3d(0, 0, -100px);
`

/**
 * Dayfour 3d Cube 6 side
 */
const DayFour = () => (
  <Wrapper>
    <CubeWrapper>
      <CubeFrontSide />
      <CubeTopSide />
      <CubeBackSide />
      <CubeDownSide />
      <CubeLeftSide />
      <CubeRightSide />
    </CubeWrapper>
  </Wrapper>
)

export default DayFour
