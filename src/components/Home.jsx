import React from 'react'
import styled, { keyframes } from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  border-bottom: 1px solid #efefef;

  background: #fff;

  @media (max-width: 900px) {
    flex-direction: column;
  }
`

const ContentWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  color: #fff;
  font-size: 28px;
  font-weight: 600;
  width: 60%;
  height: 100%;

  background: linear-gradient(to bottom, #406bd8 0%, #9cdbef 100%);

  @media (max-width: 900px) {
    width: 100%;
  }
`

const ContentTitle = styled.div`
  padding: 40px;
  font-size: 32px;
`

const TitleWrapper = styled.div`
  display: flex;
  background: #fff;
  position: relative;
  width: 40%;
  height: 100%;

  @media (max-width: 900px) {
    width: 100%;
  }
`

const Curve = styled.div`
  position: absolute;
  z-index: 100;
  height: 120%;
  top: -10%;
  width: 500px;
  left: -175px;
  border-radius: 50%;
  background: #fff;

  @media (max-width: 900px) {
    display: none;
  }
`

const SectionWrapper = styled.div`
  display: flex;
  color: #121212;
  flex: 1;
  justify-content: center;
  align-items: center;
  z-index: 200;
`

const rotateAnimation = keyframes`
    0 {
        transform: translateZ(-100px) rotateX( 70deg ) rotateZ( -45deg )
    }

    10% {
        transform: translate3d(100px, 0px,-100px) rotateX(-110deg) rotateZ(135deg)
    }

    50% {
        transform: translate3d(100px, 0px,-100px) rotateX(-110deg) rotateZ(135deg)
    }

    60% {
        transform: translateZ(-100px) rotateX( 70deg ) rotateZ( -45deg )
    }

    100% {
        transform: translateZ(-100px) rotateX( 70deg ) rotateZ( -45deg )
    }
`

const CubeWrapper = styled.div`
  position: relative;
  left: -100px;
  transform: translate3d(0, 0, -100px) rotateX(70deg) rotateZ(-45deg);
  transform-style: preserve-3d;

  animation: ${rotateAnimation} 15s ease-in-out infinite;
`

const CubeFrontSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #f5f5f5;
  transform: rotateY(0deg) translate3d(0, 0, 100px);
`

const CubeLeftSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #efefef;
  transform: rotateY(90deg) translate3d(0, 0, -100px);
`

const CubeRightSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #eeeeee;
  transform: rotateY(90deg) translate3d(0, 0, 100px);
`

const CubeTopSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #eeeeee;
  transform: rotateX(90deg) translate3d(0, 0, 100px);
`

const CubeDownSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #ddd;
  transform: rotateX(90deg) translate3d(0, 0, -100px);
`

const CubeBackSide = styled.div`
  width: 200px;
  height: 200px;
  position: absolute;
  background: #f5f5f5;
  transform: rotateY(0deg) translate3d(0, 0, -100px);
`

const ScrollDown = styled.div`
  position: absolute;
  bottom: 15px;
  left: 15px;
  font-size: 12px;
`

const Home = () => {
  return (
    <Wrapper>
      <ContentWrapper>
        <ContentTitle>Mackie css</ContentTitle>
        <ScrollDown>Scroll down</ScrollDown>
      </ContentWrapper>
      <TitleWrapper>
        <Curve />
        <SectionWrapper>
          <CubeWrapper>
            <CubeFrontSide />
            <CubeTopSide />
            <CubeBackSide />
            <CubeDownSide />
            <CubeLeftSide />
            <CubeRightSide />
          </CubeWrapper>
        </SectionWrapper>
      </TitleWrapper>
    </Wrapper>
  )
}

export default Home
