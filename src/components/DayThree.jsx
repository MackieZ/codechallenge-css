import React from 'react'
import styled, { keyframes } from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #9CDBEF;
`

const ballAnimation = keyframes`
 0 {
     transform: translateX(-150px);
 }

 50% {
     transform: translateX(150px);
 }

 100% {
     transform: translateX(-150px);
 }
`

const BouncingBall = styled.div`
  width: 50px;
  height: 50px;
  background: #fff;
  border-radius: 50%;
  position: absolute;
  transform: translateX(-150px);

  animation: ${ballAnimation} 2s ease-in-out infinite;
`

const ballBlurAnimation = keyframes`
  0 {
  	transform: translateX(-150px);
 	}

 	51% {
  	transform: translateX(150px);
 	}

 	100% {
    transform: translateX(-150px);
 	}
`

const BallBlur = styled(BouncingBall)`
  transform: translateX(-150px);
  opacity: 0.7;

  animation: ${ballBlurAnimation} 2s ease-in-out infinite;
`

const ballBlurAnimation2 = keyframes`
	0 {
		transform: translateX(-150px);
	}

	52% {
		transform: translateX(150px);
	}

	100% {
		transform: translateX(-150px);
	}
`

const BallBlur2 = styled(BouncingBall)`
  transform: translateX(-150px);
  opacity: 0.4;

  animation: ${ballBlurAnimation2} 2s ease-in-out infinite;
`

/**
 * DayThree bouning ball animation left to right
 */
const DayThree = () => (
  <Wrapper>
    <BouncingBall />
    <BallBlur />
    <BallBlur2 />
  </Wrapper>
)

export default DayThree
