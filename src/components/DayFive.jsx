import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #f5f5f5;
`

const TabsWrapper = styled.div`
  height: 50px;
  display: flex;
  width: 100%;
  position: relative;
`

const TabItem = styled.div`
  text-align: center;
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #1976d2;
  color: #fff;
  font-weight: bold;

  cursor: pointer;
`

const TabSlider = styled.div`
  display: block;
  width: ${(props) => props.length && `calc(${100 / props.length}%)`};
  position: absolute;
  height: 0;
  bottom: 0;
  border-bottom: 3px solid #e45f5f;

  left: ${(props) => props.selectedIndex && `${(props.selectedIndex * 100) / props.length}%`};
  transition: left 0.4s ease-in-out;
`

const TAB_ITEMS = ['Tab 1', 'Tab 2', 'Tab 3']

//Tabs slider
const DayFive = () => {
  const [selectedIndex, setSelectedIndex] = React.useState(0)
  const tabList = TAB_ITEMS.map((tab, index) => <TabItem onClick={() => setSelectedIndex(index)}>{tab}</TabItem>)

  return (
    <Wrapper>
      <TabsWrapper>
        {tabList}

        <TabSlider length={tabList.length} selectedIndex={selectedIndex} />
      </TabsWrapper>
    </Wrapper>
  )
}
export default DayFive
